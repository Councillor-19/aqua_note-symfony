<?php


namespace App\Controller;

use App\Entity\Genus;
use App\Entity\GenusNote;
use App\Service\MarkdownTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Michelf\MarkdownInterface;

class GenusController extends AbstractController
{

	/**
	 * @Route("/genus/new")
	 */
	public function newAction()
	{
		$genus = new Genus();
		$genus->setName('Octopus'.rand(1, 100));
		$genus->setSubFamily('Octopodiane');
		$genus->setSpeciesCount(rand(100, 99999));
		$genus->setIsPublished(true);

		$genusNote = new GenusNote();
		$genusNote->setUsername('User Name');
		$genusNote->setUserAvatarFilename('ryan.jpeg');
		$genusNote->setNote('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam');
		$genusNote->setCreatedAt(new \DateTime('-1 month'));
		$genusNote->setGenus($genus);

		$em = $this->getDoctrine()->getManager();
		$em->persist($genus);
		$em->persist($genusNote);
		$em->flush();

		return new Response("Genus created");
	}

	/**
	 * @Route("/genus")
	 */
	public function listAction()
	{
		$em = $this->getDoctrine()->getManager();

		$genuses = $em->getRepository('App:Genus')
			->findAllPublishedOrderRecentlyActive();

		return $this->render('genus/list.html.twig', compact('genuses'));
	}

	/**
	 * @Route("/genus/{genusName}", methods={"GET"}, name="genus_show")
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function showAction($genusName, MarkdownTransformer $markdown)
	{
		$em = $this->getDoctrine()->getManager();

		$genus = $em->getRepository('App:Genus')
			->findOneBy(['name' => $genusName]);

		if(!$genus) {
			throw $this->createNotFoundException('No genus found');
		}

		$funFact = $markdown->parse($genus->getFunFact());

//		$recentNotes = $genus->getNotes()
//			->filter(function (GenusNote $note) {
//				return $note->getCreatedAt() > new \DateTime('-3 month');
//			});

		$recentNotesCount = $em->getRepository('App:GenusNote')
			->findAllRecentNotesForGenus($genus);

		return $this->render('genus/show.html.twig', compact(
			'genus',
			'recentNotesCount',
			'funFact'
		));
	}

	/**
	 * @Route("/genus/{name}/notes", methods={"GET"}, name="genus_show_notes")
	 */
	public function getNotesAction(Genus $genus)
	{
		$notes = [];
		foreach ($genus->getNotes() as $note){
			$notes[] = [
				'id' => $note->getId(),
				'username' => $note->getUsername(),
				'avatarUri' => '/build/images/' . $note->getUserAvatarFilename(),
				'note' => $note->getNote(),
				'date' => $note->getCreatedAt()->format('M d, Y')
			];
		}

		$data = [
			'notes' => $notes,
		];

		return new JsonResponse($data);
	}
}