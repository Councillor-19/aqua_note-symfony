<?php


namespace App\Service;


use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class MarkdownTransformer
{
	private $markdownParser;

	public function __construct(MarkdownParserInterface $markdownParser)
	{
		$this->markdownParser = $markdownParser;
	}

	public function parse($str)
	{

		$cache = new FilesystemAdapter();
		$key = md5($str);
		$cache_item = $cache->getItem($key);

		if (!$cache_item->isHit()) {
			$value = $this->markdownParser
				->transform($str);
            $cache->save($cache_item->set($value));
		} else {
			$value = $cache_item->get();
		}

		return $value;
	}
}