<?php

namespace App\Repository;

use App\Entity\Genus;
use App\Entity\GenusNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GenusNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method GenusNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method GenusNote[]    findAll()
 * @method GenusNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenusNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GenusNote::class);
    }

    // /**
    //  * @return GenusNote[] Returns an array of GenusNote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GenusNote
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

	/**
	 * @param Genus $genus
	 *
	 * @return integer
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function findAllRecentNotesForGenus(Genus $genus)
    {
		return $this->createQueryBuilder('genus_note')
			->select('count(genus_note.id)')
			->andWhere('genus_note.genus = :genus')
			->setParameter('genus', $genus)
			->andWhere('genus_note.createdAt > :recentDate')
			->setParameter('recentDate', new \DateTime('-3 months'))
			->getQuery()
			->getSingleScalarResult();
    }
}
