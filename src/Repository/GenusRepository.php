<?php

namespace App\Repository;

use App\Entity\Genus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Genus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Genus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Genus[]    findAll()
 * @method Genus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Genus::class);
    }

    // /**
    //  * @return Genus[] Returns an array of Genus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

	/**
	 * @return Genus[] Returns an array of Genus objects
	 */
    public function findAllPublishedOrderBySize()
    {
    	return $this->createQueryBuilder('g')
		    ->andWhere('g.isPublished = :isPublished')
		    ->setParameter('isPublished', true)
		    ->orderBy('g.speciesCount', 'DESC')
		    ->getQuery()
		    ->getResult();
    }

	/**
	 * @return Genus[] Returns an array of Genus objects
	 */
	public function findAllPublishedOrderRecentlyActive()
	{
		return $this->createQueryBuilder('g')
		            ->andWhere('g.isPublished = :isPublished')
		            ->setParameter('isPublished', true)
		            ->leftJoin('g.notes', 'genus_note')
					->orderBy('genus_note.createdAt', 'DESC')
		            ->getQuery()
		            ->getResult();
	}

    /*
    public function findOneBySomeField($value): ?Genus
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
