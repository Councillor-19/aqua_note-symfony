<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GenusRepository")
 */
class Genus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string")
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\SubFamily")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $subFamily;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $speciesCount;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $funFact;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isPublished;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\GenusNote", mappedBy="genus")
	 * @ORM\OrderBy({"createdAt"="DESC"})
	 */
	private $notes;

	public function __construct()
	{
		$this->notes = new ArrayCollection();
	}

	/**
	 * @return ArrayCollection|GenusNote[]
	 */
	public function getNotes() {
		return $this->notes;
	}

	public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ): void
	{
		$this->name = $name;
	}

	/**
	 * @return SubFamily
	 */
	public function getSubFamily()
	{
		return $this->subFamily;
	}

	public function setSubFamily(SubFamily $subFamily)
	{
		$this->subFamily = $subFamily;
	}

	/**
	 * @return mixed
	 */
	public function getSpeciesCount() {
		return $this->speciesCount;
	}

	/**
	 * @param mixed $speciesCount
	 */
	public function setSpeciesCount( $speciesCount ): void {
		$this->speciesCount = $speciesCount;
	}

	/**
	 * @return mixed
	 */
	public function getFunFact() {
		return $this->funFact;
	}

	/**
	 * @param mixed $funFact
	 */
	public function setFunFact( $funFact ): void {
		$this->funFact = $funFact;
	}

	/**
	 * @return mixed
	 */
	public function getIsPublished() {
		return $this->isPublished;
	}

	/**
	 * @param mixed $isPublished
	 */
	public function setIsPublished( $isPublished ): void {
		$this->isPublished = $isPublished;
	}

	/**
	 * @return \DateTime
	 * @throws \Exception
	 */
	public function getUpdatedAt() {
		return new \DateTime('-'.rand(1,100).' days');
	}

}
