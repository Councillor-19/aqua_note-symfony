<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GenusNoteRepository")
 */
class GenusNote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userAvatarFilename;

    /**
     * @ORM\Column(type="text")
     */
    private $note;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Genus", inversedBy="notes")
	 * @ORM\JoinColumn(nullable=false)
	 */
    private $genus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUserAvatarFilename(): ?string
    {
        return $this->userAvatarFilename;
    }

    public function setUserAvatarFilename(string $userAvatarFilename): self
    {
        $this->userAvatarFilename = $userAvatarFilename;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

	/**
	 * @return Genus
	 */
	public function getGenus() {
		return $this->genus;
	}

	/**
	 * @param mixed $genus
	 */
	public function setGenus( Genus $genus ): void {
		$this->genus = $genus;
	}

}
