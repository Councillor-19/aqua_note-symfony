<?php

namespace App\DataFixtures;

use App\Entity\Genus;
use App\Entity\SubFamily;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;
use App\Entity\GenusNote;

class GenusNoteFixtures extends Fixture
{
    protected $faker;

    public function load(ObjectManager $manager)
    {
	    $generator = Factory::create();

	    $populator = new Populator($generator, $manager);

	    $populator->addEntity(SubFamily::class, 20);

	    $populator->addEntity(Genus::class, 20, array(
	    	'name' => function() use ($generator) { return $generator->firstName . '-' . $generator->randomDigit; },
		    'speciesCount' => function() use ($generator) { return $generator->numberBetween(1000, 99999); },
		    'funFact' => function() use ($generator) { return $generator->sentence; },
		    'isPublished' => function() use ($generator) {
	    		    return $generator->boolean(50);
	    		},
	    ));



	    $populator->addEntity(GenusNote::class, 100, array(
	    	'username' => function() use ($generator) { return $generator->name; },
		    'userAvatarFilename' => function() use ($generator) {
		            return $generator->randomElement([
					    'leanna.jpeg',
					    'ryan.jpeg'
			        ]);
	    		},
		    'note' => function() use ($generator) { return $generator->sentence; },
		    'createdAt' => function() use ($generator) {
	    		    return $generator->dateTimeBetween('-6 month', 'now');
	    		},
	    ));

	    $populator->execute($manager);
	    //$manager->persist();
	    //$manager->flush();

    }
}
