<?php


namespace App\Twig;

use App\Service\MarkdownTransformer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class MarkdownExtension extends AbstractExtension
{

	/**
	 * @var MarkdownTransformer
	 */
	private $markdown_transformer;

	public function __construct(MarkdownTransformer $markdown_transformer)
	{

		$this->markdown_transformer = $markdown_transformer;
	}

	public function getFilters()
	{
		return [
			new TwigFilter('markdownify', [$this, 'parseMarkdown'], ['is_safe' => ['html']]),
		];
	}

	public function parseMarkdown($str, $is_save = 'html')
	{
		return $this->markdown_transformer->parse($str);
	}
}