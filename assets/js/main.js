require('../vendor/bootstrap/css/bootstrap.min.css');
require('../css/styles.css');
require('../vendor/fontawesome/css/font-awesome.min.css');
const $ = require('jquery');
+function ($) {

    $(document).ready(function() {
        $('.js-header-search-toggle').on('click', function() {
            $('.search-bar').slideToggle();
        });
    });

}(jQuery);
